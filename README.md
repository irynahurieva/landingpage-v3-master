
# 1. Build the project 

## 1.0 Install dependencies

```
npm install
``` 

## 1.1 build for development:

```
npm run build:development
``` 

## 1.2 build for sandbox:

```
npm run build:sandbox
```

## 1.3 build for production:

```
npm run build:production
```

# 2. Developing new features or changing any code:

You can set an observable and auto rebuild project after any change by:

```
npm start
```

# 3. Run in localhost to view the site

## 3.1 If you like to use terminal

Run in terminal:

```
dev_appserver.py build/app.yaml
```

You can found more details here about how to run local server in terminal:

https://cloud.google.com/appengine/docs/standard/python/tools/using-local-server

## 3.2 If you don't like terminal

To view the site, run the folder `/build` in the AppEngineLauncher application.

Download this application here:

MAC: https://storage.googleapis.com/appengine-sdks/featured/GoogleAppEngineLauncher-1.9.83.dmg

WINDOWNS: https://storage.googleapis.com/appengine-sdks/featured/GoogleAppEngine-1.9.83.msi

You can found more details here about how to run local server in application:

https://cloud.google.com/appengine/docs/standard/python/download


# 4. Deploy

Before deploy the site to Appengine, be sure to build it for the right environment.
