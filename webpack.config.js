const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

const devMode = process.env.NODE_ENV !== "production"

module.exports = {
  watch: devMode,
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: devMode ? "main.js" : "main.[hash].js",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      title: "Stark Bank",
      favicon: "./src/icon.ico",
    }),
    new MiniCssExtractPlugin({
      filename: devMode ? "[name].css" : "[name].[hash].css",
      chunkFilename: devMode ? "[id].css" : "[id].[hash].css",
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: "babel-loader",
      },
      {
        test: /\.styl|\.css$/,
        use: [
          devMode ? "style-loader" : MiniCssExtractPlugin.loader,
          "css-loader",
          "stylus-loader",
        ],
      },
      {
        test: /\.(jpg|png|svg|gif)$/,
        loader: "url-loader",
        options: {
          limit: 8192,
          name: "./images/[name].[ext]",
        },
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader",
        options: {
          limit: 50000,
          mimetype: "application/font-woff",
          name: "./fonts/[name].[ext]",
        },
      },
      {
        test: /app\.yaml$/,
        loader: "file-loader",
        options: {
          name: "./app.yaml",
        },
      },
      {
        test: /\.html$/,
        exclude: /index\.html/,
        loader: "file-loader",
        options: {
          name: "./htmls/[name].html",
        },
      },
    ],
  },
}
