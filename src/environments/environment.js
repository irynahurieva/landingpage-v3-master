import environment from "./environment.json"

export const Environment = new _Environment()

function _Environment() {

    this.data = environment

    this.get = function(key) {
        return this.data[key]
    }
}
