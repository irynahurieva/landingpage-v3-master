

export class LanguageGateway {

    static setLanguage(language) {
        localStorage.setItem("language", language)
    }

    static getLanguage() {
        let language = navigator.language
        let defaultLanguage = {
            "pt-BR": "pt-BR",
            "pt-br": "pt-BR",
            "pt": "pt-BR",
        }[language] || "en-US"

        return localStorage.getItem("language") || defaultLanguage
    }
}
