import "../components"


function Route(regex, html) {
    if (!(this instanceof Route)) return new Route(regex, html)
    this.regex = regex
    this.html = html
}

export function routes() {
    var path = window.location.pathname

    var routes = [
        Route("^/$", "homeView.html"),
        Route("^/about-us$", "aboutView.html"),
        Route("^/pricing$", "pricingView.html"),
        Route("^/our-solution$", "solutionView.html")
    ]

    var htmlName = "notFoundView.html"
    routes.forEach(function(route) {
        if (path.match(route.regex)) {
            htmlName = route.html
            return
        }
    })

    $("#container").load(`/htmls/${htmlName}`);
}
