import "./app.yaml"
import "./images"
import "./utils"
import { routes } from "./routes/routes"

routes()
