import { LanguageGateway } from "../../gateways/languageGateway"


export const aboutLanguage = {
    "en-US": {
        "title": "Innovating the banking in Brazil",
        "subtitle": "Stark Bank is a tool that helps companies to simplify the process of make wire transfers and slips in Brazil. Have everything in one place at the cost of pennies.",
    },
    "pt-BR": {
        "title": "Inovando o banking no Brasil",
        "subtitle": "Stark Bank é uma ferramenta que ajuda empresas a simplificar o processo de realizar transferências e boletos no Brasil. Tenha tudo em um único lugar ao custo de centavos.",
    },
}[LanguageGateway.getLanguage()]
