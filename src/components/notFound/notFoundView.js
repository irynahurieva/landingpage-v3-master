import { UILabel } from "../../utils/cocoatouch/uikit"
import { notFoundLanguage } from "./notFoundLanguage"


class NotFoundView {

    titleLabel = new UILabel(".not-found-title")

    viewDidLoad() {
        this.setLanguage()
    }

    setLanguage() {
        this.titleLabel.setText(notFoundLanguage["title"])
    }
}

window.NotFoundView = NotFoundView
