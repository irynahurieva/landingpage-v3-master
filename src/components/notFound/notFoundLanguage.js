import { LanguageGateway } from "../../gateways/languageGateway"


export const notFoundLanguage = {
    "en-US": {
        "title": "Page not Found",
        "subtitle": "",
    },
    "pt-BR": {
        "title": "Página não encontrada",
        "subtitle": "",
    },
}[LanguageGateway.getLanguage()]
