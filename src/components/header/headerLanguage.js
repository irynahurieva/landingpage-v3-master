import { LanguageGateway } from "../../gateways/languageGateway"


export const headerLanguage = {
    "en-US": {
        "about-us": "About Us",
        "our-solution": "Our Solution",
        "api": "API",
        "pricing": "Pricing",
        "signin": "LOG IN",
        "signup": "JOIN US"
    },
    "pt-BR": {
        "about-us": "Sobre nós",
        "our-solution": "Nossa Solução",
        "api": "API",
        "pricing": "Preços",
        "signin": "ENTRAR",
        "signup": "CADASTRAR"
    },
}[LanguageGateway.getLanguage()]
