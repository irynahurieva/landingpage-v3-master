import { getHtml } from "../../utils/dom"


export class HeaderPresenter {

    static userDidSelect(id, view) {
        var nextPage = {
            "header-our-solution": {
                "path": "/our-solution",
                "html": "./htmls/solutionContentView.html",
                "selector": `#${id}`
            },
            "header-about-us": {
                "path": "/about-us",
                "html": "./htmls/aboutContentView.html",
                "selector": `#${id}`
            },
            "header-pricing": {
                "path": "/pricing",
                "html": "./htmls/pricingContentView.html",
                "selector": `#${id}`
            },
            "header-logo": {
                "path": "/",
                "html": "./htmls/homeContentView.html",
                "selector": null
            }
        }[id] || { "html": null, "path": "/", "selector": `#${id}` }

        view.deselectAll()

        var selector = nextPage["selector"]
        if (selector) {
            view.setSelected(selector)
        }

        const container = "#home-content"
        const html = nextPage["html"]
        if (html) {
            $(container).css("visibility", "hidden");
            $(container).load(html,  function() {
                $(container).delay(25).queue(function (next) {
                    $(container).css("visibility", "visible");
                    next();
                });
            });
        } else {
            $("#home-content").empty();
        }

        const path = nextPage["path"]
        window.history.pushState(path, "Stark Bank", path);
    }
}


