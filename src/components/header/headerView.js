import { UIButton } from "../../utils/cocoatouch/uikit"
import { HeaderPresenter } from "./headerPresenter"
import { headerLanguage } from "./headerLanguage"


class HeaderView {

    aboutLabel = new UIButton("#header-about-us")
    pricingLabel = new UIButton("#header-pricing")
    solutionLabel = new UIButton("#header-our-solution")
    apiLabel = new UIButton("#header-api")
    signinLabel = new UIButton("#header-signin")
    signupLabel = new UIButton("#header-signup")


    viewDidLoad() {
        this.setLanguage()
    }

    setLanguage() {
        this.aboutLabel.setText(headerLanguage["about-us"])
        this.pricingLabel.setText(headerLanguage["pricing"])
        this.solutionLabel.setText(headerLanguage["our-solution"])
        this.apiLabel.setText(headerLanguage["api"])
        this.signinLabel.setText(headerLanguage["signin"])
        this.signupLabel.setText(headerLanguage["signup"])
    }

    userDidSelect(id, text) {
        HeaderPresenter.userDidSelect(id, this)
    }

    redirect(url) {
        window.location.href = url
    }

    setSelected(selector) {
        new UIButton(selector).setStyle("header-selected")
    }

    deselectAll() {
        var buttons = [this.aboutLabel, this.pricingLabel, this.solutionLabel]
        for(let button of buttons) {
            button.setStyle("header-unselected")
        }
    }
}

window.HeaderView = HeaderView
