import { UIView, UILabel } from "../../utils/cocoatouch/uikit"


class NotificationView {

    view = new UIView(".notification-container")
    subview = new UIView(".notification")
    label = new UILabel("#notification-text")

    viewDidAppear() {
        this.view.isHidden = true
    }

    showErrorText(text) {
        this.subview.setStyle("notification notification-failed")
        this.__setText(text)
    }

    showSuccessText(text) {
        this.subview.setStyle("notification notification-success")
        this.__setText(text)
    }

    __setText(text) {
        this.label.setText(text)
        this.view.isHidden = false
        setTimeout(() => { this.__dismiss() }, 5000);
    }

    __dismiss() {
        this.view.isHidden = true
    }
}

window.NotificationView = NotificationView
