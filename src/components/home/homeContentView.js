import { UILabel } from "../../utils/cocoatouch/uikit"
import { homeLanguage } from "./homeLanguage"


class HomeContentView {

    titleLabel = new UILabel(".home-title")
    subtitleLabel = new UILabel(".home-subtitle")

    viewDidLoad() {
        this.setLanguage()
    }

    setLanguage() {
        this.titleLabel.setText(homeLanguage["title"])
        this.subtitleLabel.setText(homeLanguage["subtitle"])
    }
}

window.HomeContentView = HomeContentView
