

export function DateFormat() {

    this.format = function(date) {
        var DD = date.getDate()
        var MM = date.getMonth() + 1
        var YYYY = date.getFullYear()
        var hh = date.getHours()
        var mm = date.getMinutes()

        DD = DD < 10 ? "0" + DD : DD
        MM = MM < 10 ? "0" + MM : MM
        hh = hh < 10 ? "0" + hh : hh
        mm = mm < 10 ? "0" + mm : mm
        var YY = YYYY - 2000

        return DD + "/" + MM + "/" + YY + " - " + hh + ":" + mm
    }
}
