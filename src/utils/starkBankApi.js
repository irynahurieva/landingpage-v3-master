import { Request, Response } from "./request"
import { SessionGateway } from "../gateways/sessionGateway"
import { Environment } from "../environments/environment"


export function StarkBankApi() {

    var baseUrl = function() {
        return Environment.get("baseUrl")
    }

    var defaultHeaders = function() {
        return {
            "Content-Type": "application/json",
            "Accept-Language": "pt-BR",
            "Access-Token": SessionGateway.getAccessToken()
        }
    }

    var handle = function(response, callback) {
        if (response.status === 400 && response.json()["error"]["code"] === "invalidAccessToken") {
            window.location.href = "/signin"
            return
        }
        callback(response)
    }

    this.get = function(path, query, headers, callback) {
        var finalHeaders = $.extend( {}, defaultHeaders(), headers )
        new Request().fetch(
            `${baseUrl()}${path}${query}`, {
                "method": "GET",
                "headers": finalHeaders
            },
            function (response) {
                handle(response, callback)
            }
        )
    }

    this.post = function(path, payload, headers, callback) {
        var finalHeaders = $.extend( {}, defaultHeaders(), headers )
        new Request().fetch(
            `${baseUrl()}${path}`, {
                "method": "POST",
                "body": JSON.stringify(payload),
                "headers": finalHeaders
            },
            function (response) {
                handle(response, callback)
            }
        )
    }
}
