

export function Currency() {

    this.format = function(value) {
        var options = { minimumFractionDigits: 2 }
        return (value/100.0).toLocaleString("pt-BR", options)
    }
}
