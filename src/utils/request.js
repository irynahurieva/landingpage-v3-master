

export function Response() {

    this.status = -1
    this.content = ""

    this.json = function() {
        return this.content || {}
    }

    this.error = function() {
        if (this.status === 200) { return {} }
        return this.json()["error"]
    }
}


export function Request() {

    this.fetch = function(url, options, callback) {
        fetch(url, options).then(function(response) {
            var status = response.status

            response.json().then(function(content) {
                const result = new Response()
                result.content = content
                result.status = status
                callback(result)
            })
        }).catch(function(error) {
            const result = new Response()
            result.content = {"error": {"message": "Verifique sua conexão com a Internet"}}
            result.status = 500
            callback(result)
        });
    }
}
