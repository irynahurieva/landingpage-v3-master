
export function getElement(selector, callback){
    var poller = setInterval(function(){
        $jObject = jQuery(selector);
        if($jObject.length < 1){
            return;
        }
        clearInterval(poller);
        callback($jObject)
    },10);
}

export function getHtml(htmlPath, callback) {
    var tempDiv = $("<div></div>").addClass("hidden")
    tempDiv.load(htmlPath, function() {
        callback(tempDiv.html())
        tempDiv.remove()
    });
}
