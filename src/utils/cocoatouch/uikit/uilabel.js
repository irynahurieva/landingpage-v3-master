import { UIView } from "./uiview"


export class UILabel extends UIView {

    setText(text) {
        this.__element.innerHTML = text
    }
}