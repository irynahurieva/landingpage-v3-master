

export class UIView {

    constructor(selector) {
        this.selector = selector
    }

    set isHidden(bool) {
        this.__element.style.display = bool ?  "none" : "block"
    }

    get isHidden() {
        return this.__element.style.display === "none"
    }

    setStyle(style) {
        this.__element.className = style
    }

    get __element() {
        return document.querySelector(this.selector)
    }
}