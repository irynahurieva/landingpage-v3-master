import { UIView } from "./uiview"


export class UITextField extends UIView {

    text() {
        return this.__element.value
    }

    becomeFirstResponder() {
        $(this.selector).focus()
    }

    resignFirstResponder() {
        $(this.selector).blur()
    }
}