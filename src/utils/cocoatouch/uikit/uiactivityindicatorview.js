import { UIView } from "./uiview"


export class UIActivityIndicatorView extends UIView {

    startAnimating() {
        this.isHidden = false
    }

    stopAnimating() {
        this.isHidden = true
    }
}