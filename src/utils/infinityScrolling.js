

export function infinityScrolling(callback) {
    var lastTarget = 0
    window.addEventListener("scroll", function(event) {
        var currentYOffset = window.pageYOffset
        var maxHeight = document.documentElement.scrollHeight
        var browserHeight = document.documentElement.clientHeight
        var delta = 0
        var targetYOffset = maxHeight - browserHeight - delta

        if(currentYOffset === targetYOffset && lastTarget != targetYOffset) {
            lastTarget = targetYOffset
            callback()
        }
    })
}